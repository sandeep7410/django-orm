from django.db import models
from django.conf import settings


# Create your models here.
class Book(models.Model):
    title=models.CharField(max_length=75)
    price=models.IntegerField()

    def __str__(self):
        return self.title

class Category(models.Model):
    name=models.CharField(max_length=50)

    def __str__(self):
        return self.name

class BookCategory(models.Model):
    book=models.ForeignKey(Book,on_delete=models.CASCADE,related_name='categories_by_book')
    category=models.ForeignKey(Category,on_delete=models.CASCADE,related_name='books_by_category')

    def __str__(self):
        return self.category.name

class BookAuthor(models.Model):
    book=models.ForeignKey(Book,on_delete=models.CASCADE,related_name='authors_by_book')
    author=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,related_name='books_by_author')

    def __str__(self):
        return self.author.username

class Cart(models.Model):
    user=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,related_name='user_cart')
    
    def __str__(self):
        return self.user.username

class CartItem(models.Model):
    cart=models.ForeignKey(Cart,on_delete=models.CASCADE,related_name='user_cart_item')
    book=models.ForeignKey(Book,on_delete=models.CASCADE,related_name='filter_by_book')
    quantity=models.IntegerField()

    def __str__(self):
        return str(self.cart.id)+self.book.title
    
    