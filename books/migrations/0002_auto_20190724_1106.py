# Generated by Django 2.2.3 on 2019-07-24 11:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookauthor',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='books_by_author', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='bookauthor',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='authors_by_book', to='books.Book'),
        ),
        migrations.AlterField(
            model_name='bookcategory',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='categories_by_book', to='books.Book'),
        ),
        migrations.AlterField(
            model_name='bookcategory',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='books_by_category', to='books.Category'),
        ),
    ]
