from django.db.models import Count, Sum, Max
from books.models import Book, BookAuthor, BookCategory, Category, Cart, CartItem
from django.conf import settings
from django.contrib.auth.models import User
import random


def get_book_by_id(book_id):
    b = Book.objects.get(id=book_id)


def get_books_in_list(li):
    b = Book.objects.filter(id__in=li).all()


def filter_books_by_category(category_name):
    c = BookCategory.objects.filter(category__name=category_name).all()


def no_of_books_in_category(category_name):
    c = BookCategory.objects.filter(category__name=category_name).all().count()


def author_of_max_books():
    a = BookAuthor.objects.all().values('author').annotate(
        total=Count('author')).aggregate(Max('total'))


def cost_of_all_books():
    p = Book.objects.all().aggregate(Sum('price'))


def most_expensive_book():
    p = Book.objects.all().order_by('-price')[:1]


def books_costlier_than(price):
    p = Book.objects.all().filter(price__gt=price).order_by('-price')


def delete_by_id(book_id):
    d = Book.objects.filter(id=book_id).delete()


def get_cartitems_by_cart_id(cart_id):
    c = CartItem.objects.filter(cart__id=cart_id).select_related(
        'cart__user', 'book').all()
