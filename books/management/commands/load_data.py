from django.core.management.base import BaseCommand, CommandError
from books.models import Book, BookAuthor, BookCategory, Category, Cart, CartItem
from django.conf import settings
from django.contrib.auth.models import User
import random


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for i in range(1, 11):
            User.objects.create_user(username="u"+str(i), password="u"+str(i))

        for i in range(1, 11):
            category = Category(name='c'+str(i))
            category.save()
        for i in range(1, 41):
            r = random.randint(1, 100)
            book = Book(title='b'+str(i), price=r)
            book.save()
            create_authors(book, 2)
            create_categories(book, 3)
        users = User.objects.all()
        books = Book.objects.all()
        for i in range(1, 11):
            create_cart(users, books, 2)

        self.stdout.write(self.style.SUCCESS('Successfully closed '))


def create_categories(book, j):
    for i in range(0, j):
        category = Category.objects.get(id=random.randint(1, 10))
        bookcategory = BookCategory(book=book, category=category)
        bookcategory.save()
        i = i+1


def create_authors(book, j):
    for i in range(0, j):
        author = User.objects.get(id=random.randint(1, 10))
        book_author = BookAuthor(book=book, author=author)
        book_author.save()
        i = i+1


def create_cart(users, books, j):
    for i in range(0, j):
        cart = Cart.objects.create(user=random.choice(users))
        for k in range(1, 4):
            CartItem.objects.create(cart=cart, book=random.choice(
                books), quantity=random.randint(1, 5))
            k = k+1
        i = i+1
